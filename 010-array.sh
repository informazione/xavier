#!/bin/bash 

ARRAY=('pippo' 'pluto' 'paperino');

echo ${ARRAY[*]};

echo ${ARRAY[2]}

ARRAY[3]='paperoga';

echo ${ARRAY[*]};

ARRAY[4]='';
ARRAY[5]='topolino';

echo ${ARRAY[*]};
echo ${ARRAY[4]}
echo ${ARRAY[5]}

echo '--------------------';

# Number of elements in the array
ELEMENTS=${#ARRAY[*]}

echo $ELEMENTS;

for ((i=0; i<$ELEMENTS; i++)); do
    echo $i;
    echo ${ARRAY[${i}]};
done;
