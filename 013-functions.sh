#!/bin/bash

echo 'pluto';

function hello {
    echo 'Hello world';
    echo 'Happy hacking';
}

function say_hello_to {
    echo "Hello" $1;
    echo "Hello" $2;
}

function area_triangle {
    local AREA;
    let AREA="$1 * $2 / 2";
    echo $AREA;
}


echo $AREA;

hello;
echo 'pippo';

hello;
hello;
say_hello_to "fabio";
say_hello_to "fabio" "matteo";
area_triangle 10 10;
echo $AREA;

RESULT=$(area_triangle 13 10);
echo $RESULT;

echo $AREA;

