#!/bin/bash

declare -a USERS;

exec 10<&0;
exec < $1; 
let COUNT=0;

while read LINE; do
    USERS[$COUNT]=$LINE;
    ((COUNT++));
done; 


USER_NUMBERS=${#USERS[*]};

for ((i=0;i<$USER_NUMBERS;i++)); do
    echo ${USERS[${i}]};
done; 

echo ${USERS[*]};

exec 0<&10 10<&- 
