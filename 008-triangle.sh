#!/bin/bash

#BASE=11;
#HEIGHT=2

echo 'Insert base:';
read BASE;

echo 'Insert height:';
read HEIGHT

echo '-----------------------------';

AREA=`expr $BASE \* $HEIGHT / 2`;
echo "Area with expr is: $AREA";

let AREA="$BASE * $HEIGHT / 2";
echo "Area with let is: $AREA";

AREA=$(($BASE * $HEIGHT / 2));
echo 'Area with $(()) is: '$AREA;
